package com.example.demo.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//此類別為Spring Controller元件
@RestController
public class HelloController {
	
	//透過 @RequestMapping 指定從/hello會被對應到此hello()方法
	@RequestMapping("/hello")
	//透過 @ResponseBody 告知Spring，此函數的回傳值是HTTP Response的本文
	public @ResponseBody String hello()
	{
		return "hello world";
	}
	
	@RequestMapping("/hello/{name}")
	public @ResponseBody String helloWithName(@PathVariable("name") String name)
	{
		return "hello " + name;
	}
}