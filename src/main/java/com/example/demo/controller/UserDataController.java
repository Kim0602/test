package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.UserData;
import com.example.demo.service.UserDataService;

//此類別為Spring Controller元件
@RestController
public class UserDataController {

	@Autowired
	private UserDataService userDataService;

	// 透過 @RequestMapping 指定從/hello會被對應到此hello()方法
	@RequestMapping("/user")
	// 透過 @ResponseBody 告知Spring，此函數的回傳值是HTTP Response的本文
	public @ResponseBody String hello() {
		return "hello world";
	}

	@RequestMapping("/user/{id}")
	public ResponseEntity<UserData> getUserDataWithId(@PathVariable("id") int id) {

		UserData result = userDataService.getUserDataById(id);

		return new ResponseEntity<UserData>(result, HttpStatus.OK);
	}
}