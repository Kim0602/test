package com.example.demo.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.example.demo.entity.UserData;

@Transactional
@Repository
public class UserDataDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public UserData getUserDataById(int id) {
		System.out.println("id : " + id);
		return entityManager.find(UserData.class, id);
	}

}
