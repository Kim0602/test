package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.UserDataDao;
import com.example.demo.entity.UserData;

@Service
public class UserDataService {
	@Autowired
	private UserDataDao userDataDao;
	
	public UserData getUserDataById(int id) {
		
		UserData result = userDataDao.getUserDataById(id);
		
		return result;
	}
	
	
	
}